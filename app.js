const express = require('express');
const app = express();
const PORT = 3000;

/**
* This function calculates the product of two numbers.
* @param {number} multiplicand first number to be multiplied
* @param {number} multiplier second number to be multiplied
* @returns {number} product
*/
const multiply = (multiplicand, multiplier) => {
    const result = multiplicand * multiplier;
    return result;
};

// GET Endpoint - http://localhost:3000/
app.get('/', (req, res) => {
    res.send('Hello World!');
});

// GET multiply endpoint - http://127.0.0.1:3000/multiply?a=3&b=5
app.get('/multiply', (req, res) => {
    try {
        const multiplicant = parseFloat(req.query.a);
        const multiplier = parseFloat(req.query.b);
        if (isNaN(multiplicant)) throw new Error('Invalid multiplicant value');
        if (isNaN(multiplier)) throw new Error('Invalid multiplier value');
        console.log({ multiplicant, multiplier });
        const product = multiply(multiplicant, multiplier);
        res.send("\n" + product.toString(10));
    } catch (err) {
        console.error(err.message);
        res.send("Couldn't calculate the product. Try again.");
    }
});

app.listen(PORT, () => console.log(`Listening at http://localhost:${PORT}`));